package main

import (
	"bufio"
	"context"
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"
	"runtime"
	"strings"
	"time"

	"github.com/ztrue/tracerr"
)

//http://engblog.yext.com/post/going-all-in-with-go-for-cli-apps
//https://code.tutsplus.com/tutorials/lets-go-command-line-programs-with-golang--cms-26341
func main() {
	gitdock()

	//execute()
}

//asks git or dock?
func gitdock() {

	fmt.Print(dirs(), " docker? or git?:")

	//actual command running
	scanner3 := bufio.NewScanner(os.Stdin)
	for scanner3.Scan() {

		gitdock := scanner3.Text()
		switch gitdock {
		case "git":
			git()
		case "docker":
			dock()

		case "d":
			dock()
		}
	}
}

//looks for docker commands
func dock() {
	commanddocker := flag.String("docker", "", "The docker command")
	var dockercomponents []string

	//split text
	for _, component := range strings.Split(*commanddocker, " ") {
		dockercomponents = append(dockercomponents, component)
	}

	dockercommandstring := "docker " + *commanddocker

	fmt.Printf(" %s\n", dockercommandstring)
	fmt.Print(dirs(), " docker command?:")

	//actual command running
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {

		docks := scanner.Text()

		// Execute the command
		out2, err := exec.Command("docker", docks).CombinedOutput()
		// Print the result
		fmt.Println("this is out2 ", string(out2))

		if err != nil {
			tracerr.PrintSourceColor(err, 9)
		}
		fmt.Print(dirs(), " docker command? (type git for git commands):")

		switch docks {
		case "git":
			git()
		}

	}
	gitdock()
}

//looks for git commands
func git() {
	commandgit := flag.String("git", "", "The git command")
	var gitcomponents []string

	//split text
	for _, component := range strings.Split(*commandgit, " ") {
		gitcomponents = append(gitcomponents, component)
	}
	commandstring := "git " + *commandgit

	// Print the command
	fmt.Printf(" %s\n", commandstring)
	fmt.Print(dirs(), " git command?:")

	//actual command running
	scanner2 := bufio.NewScanner(os.Stdin)
	for scanner2.Scan() {

		line := scanner2.Text()
		// Execute the command

		out, err := exec.Command("git", line).CombinedOutput()
		// Print the result
		fmt.Println(string(out))

		if err != nil {
			tracerr.PrintSourceColor(err, 9)
		}
		fmt.Print(dirs(), " git command? (type docker for docker commands):")

		switch line {
		case "dock":
			dock()
		}
	}
	gitdock()
}
func inputs() {
	dir, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Print(dir, " Current cmd: ls, check, status, add, com, ex \n")
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		line := scanner.Text()

		switch line {
		case "ls":

			ls()
			fmt.Println("ls done.....")
		case "check":
			check()
			fmt.Println("check done.....")
		case "status":
			status()
			fmt.Println("status done.....")
		case "add":
			status()
			add()
			fmt.Println("add done.....")
		case "com":
			fmt.Print(dir, " Enter Message:")
			message := scanner.Text()
			com(message)
			fmt.Println("com done.....")
		case "rm":
			rm()
			fmt.Println("rm done.....")
		case "cmd":
			cmd()
		case "ex":
			fmt.Println("exit done.....")
			exits()

		}

		fmt.Println(dir, " Enter commands:")
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}
func rm() {

	fmt.Print(dirs(), " Whats your file name?")
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		file := scanner.Text()
		cmd := exec.Command(dirs(), `git rm -cached `+file)
		err := cmd.Run()

		if err != nil {
			tracerr.PrintSourceColor(err, 9)
		}
	}

}
func add() {

	fmt.Print(dirs(), " Whats your file name?")
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		//file := scanner.Text()
		cmd := exec.Command(dirs(), "git add .")
		fmt.Println(cmd)
		err := cmd.Run()

		if err != nil {
			tracerr.PrintSourceColor(err, 9)
		}
	}

}
func com(m string) {
	dir, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	cmd := exec.Command(dir, ` "git commit -m {{.m}}`)
	err = cmd.Run()

	if err != nil {
		tracerr.PrintSourceColor(err, 9)
	}
}
func status() {
	ctx, cancel := context.WithTimeout(context.Background(), 100*time.Millisecond)
	defer cancel()

	out, err := exec.CommandContext(ctx, "git", "status").Output()
	if err != nil {
		tracerr.PrintSourceColor(err, 9)
	}
	fmt.Printf("Output %s\n", out)
}
func check() {
	fmt.Print("Go runs on ")
	switch os := runtime.GOOS; os {
	case "darwin":
		fmt.Println("OS X.")
	case "linux":
		fmt.Println("Linux.")
		execute()
	default:
		fmt.Println("Can't Execute this on a windows machine")
		fmt.Printf("%s.\n", os)
	}
}

func ls() {
	fmt.Println("ls running")
	out, err := exec.Command("ls").Output()
	if err != nil {
		tracerr.PrintSourceColor(err, 9)
	}
	fmt.Printf("Output %s\n", out)
}
func exits() {

	os.Exit(0)

}
func execute() {

	inputs()

}
func pwd() {

	out, err := exec.Command("pwd").Output()
	if err != nil {
		tracerr.PrintSourceColor(err, 9)
	}
	fmt.Printf("%s\n", out)
}
func cmd() {
	dir, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Print(dir, " Current cmd: ls, check, status, add, com, ex \n")
}
func dirs() string {
	dir, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	return dir
}
